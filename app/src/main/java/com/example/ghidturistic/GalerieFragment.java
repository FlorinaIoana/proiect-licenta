package com.example.ghidturistic;

import android.content.Context;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;




public class GalerieFragment extends Fragment  {


    public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {

         View vw= inflater.inflate(R.layout.fragment_galerie, container, false);

        ImageView imageView = (ImageView)vw.findViewById(R.id.imageView);
      imageView.setImageResource(R.drawable.mocanita);
        return vw;
    }
}


