package com.example.ghidturistic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import androidx.fragment.app.Fragment;


public class FacebookFragment extends Fragment {

    public FacebookFragment(){}


  public View onCreateView( LayoutInflater inflater,  ViewGroup container,  Bundle savedInstanceState) {

View  view =  inflater.inflate(R.layout.fragment_facebook, container, false);
WebView webView =(WebView)view.findViewById(R.id.webView);
webView.getSettings().setJavaScriptEnabled(true);
webView.setWebViewClient(new WebViewClient());
webView.loadUrl("https://www.facebook.com/");
  return  view;
    }
}
