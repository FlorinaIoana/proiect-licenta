package com.example.ghidturistic;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;
import androidx.fragment.app.ListFragment;


public class ListaFragment extends ListFragment {
  ListView listView;

  ArrayAdapter<String> arrayAdapter;

  String[] item;

  public ListaFragment() {
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = inflater.inflate(R.layout.fragment_lista, container, false);
    listView = (ListView) view.findViewById(android.R.id.list);

    item = ((MainActivity)getActivity()).item;
    arrayAdapter = new ArrayAdapter<String>(inflater.getContext(), android.R.layout.simple_list_item_1, item);
    setListAdapter(arrayAdapter);


    return view;

  }


  @Override
  public void onListItemClick(ListView l, View v, int position, long id) {
    // TODO Auto-generated method stub
    super.onListItemClick(l, v, position, id);

      ((MainActivity)getActivity()).navigatetoDetailView(position);

     Toast.makeText(getActivity(), "selected item :" + item[position],
              Toast.LENGTH_LONG).show();
  }
}
