package com.example.ghidturistic;


import android.os.Bundle;
import android.view.MenuItem;

import com.google.android.material.navigation.NavigationView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.navigation.NavController;
import androidx.navigation.Navigation;
import androidx.navigation.ui.NavigationUI;

import static androidx.navigation.ui.NavigationUI.*;

public class MainActivity extends AppCompatActivity implements
        NavigationView.OnNavigationItemSelectedListener {

    public DrawerLayout drawerLayout;

    public NavController navController;

    public NavigationView navigationView;

    public String[] item = {"Alba Iulia", "Sighisoara", "Castelul Peles"};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setupNavigation();

    }


    private void setupNavigation() {


        drawerLayout = findViewById(R.id.drawer_layout);

        navigationView = findViewById(R.id.navigationView);

        navController = Navigation.findNavController(this, R.id.nav_host_fragment);

        setupActionBarWithNavController(this, navController, drawerLayout);

        setupWithNavController(navigationView, navController);

        navigationView.setNavigationItemSelectedListener(this);

    }

    @Override
    public boolean onSupportNavigateUp() {
        return NavigationUI.navigateUp(drawerLayout, Navigation.findNavController(this, R.id.nav_host_fragment));
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem menuItem) {

        menuItem.setChecked(true);

        drawerLayout.closeDrawers();

        int id = menuItem.getItemId();
        switch (id) {

            case R.id.listaObiective:
                navController.navigate(R.id.listaFragment);
                break;

            case R.id.galerie:
                navController.navigate(R.id.galerieFragment);
                break;

            case R.id.googleMaps:
                navController.navigate(R.id.googleMapsFragment);
                break;

            case R.id.facebook:
                navController.navigate(R.id.facebookFragment);
                break;


        }
        return true;

    }

    public void navigatetoDetailView(int position){
        Bundle bundle = new Bundle();
        bundle.putInt("position",position);
        navController.navigate(R.id.detailFragment,bundle);
    }
}
