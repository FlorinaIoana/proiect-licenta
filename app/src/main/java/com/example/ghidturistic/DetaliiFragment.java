package com.example.ghidturistic;


import android.os.Bundle;

import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;


/**
 * A simple {@link Fragment} subclass.
 */
public class DetaliiFragment extends Fragment {


    public DetaliiFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {


        View viewDetalii =inflater.inflate(R.layout.fragment_detalii, container, false);
        TextView textView = (TextView)viewDetalii.findViewById(R.id.tv);

        Bundle bundle = getArguments();

        return viewDetalii;
    }
}
